"""
Usage:
    python setup.py py2exe
"""

from distutils.core import setup
import py2exe, os

SOURCES = [os.path.join('src', p) for p in os.listdir('src') if os.path.splitext(p)[1] == '.py']

DATA_FILES = [('.', SOURCES)]
INCLUDES = ['dircache', 'sets', 'shutil', 'random', 'pygame']
    
OPTIONS = {
    "py2exe": {
    "includes": INCLUDES,
    }
}
    
setup(
    version = "02",
    windows = [
        {
            "script": 'src/Main.py',
            "icon_resources": [(1, "media/krank.ico")]
        }
    ],
    data_files = DATA_FILES,
    options = OPTIONS,
)

